from flask import Flask, jsonify

from database import sequence
from blastddbb import blastp, check_subdirectories


app = Flask(__name__, static_folder="out", static_url_path="/")


@app.route("/")
def index():
    return app.send_static_file("index.html")

@app.route("/api/time")
def get_time():
    return {"time": "Hello World"}
   

@app.route('/api/blastp/<id>')
def search_blastp(id):
    sequence_file = sequence(id)
    
    if sequence_file:
        blastp_results = blastp(sequence_file)

        if blastp_results:
            formatted_results = {
                "accession_number": id,
                "blast_results": blastp_results
            }
            return jsonify(formatted_results)
        else:
            return jsonify({"error": "No se encontraron resultados para la búsqueda BLASTP"}), 404
    else:
        return jsonify({"error": "No se pudo encontrar la secuencia o el accession number de la PDB para el ID proporcionado"}), 404
   


if __name__ == "__main__":
    app.run()
