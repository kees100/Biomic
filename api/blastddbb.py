import os
import json
import subprocess
from python_on_whales import docker


CWD = os.getcwd()

base_dir = "blast"
subdirs = ["blastdb", "blastdb_custom", "fasta", "queries", "results"]

def check_subdirectories(base_dir, subdirs):
    if not os.path.exists(base_dir):
        os.mkdir(base_dir)

    for subdir in subdirs:
        subdir_path = os.path.join(base_dir, subdir)
        if not os.path.exists(subdir_path):
            os.mkdir(subdir_path)

# def update_blast_db():
#     # working_dir o cd
#     # lo demés = que __blast rm pdbaa*
#     docker.run(["update_blastdb.pl", "-source", "gcp", "-db", "pdbaa"])

   
def blastp(accession_number):

    base_dir = CWD
    
    query = f"queries/{accession_number}.fasta"
    command = ["blastp", "-query", query, "-db", "pdbaa", "-outfmt", "15"]

    output = docker.run(
        image="ncbi/blast",
        remove=True,
        volumes=[(f"{base_dir}/blast/blastdb", "/blast/blastdb"),
                 (f"{base_dir}/blast/blastdb_custom", "/blast/blastdb_custom"),
                 (f"{base_dir}/blast/fasta", "/blast/fasta"),
                 (f"{base_dir}/blast/queries", "/blast/queries")],
        command=command)
    return json.loads(output) if output else None


