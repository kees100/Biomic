from Bio import Entrez, SeqIO
import os

Entrez.email = "kees1000@office.proven.cat"

   

def sequence(id):
    

    dir = "blast/fasta"
    if not os.path.isdir(dir):
        os.makedirs(dir)

    file = "{}/{}.fasta".format(dir, id)
    if not os.path.exists(file):
        print(f"Downloading file {id}...")
        with Entrez.efetch("protein", id=id, rettype="fasta") as response:
            with open(file, "w") as out:
                out.write(response.read())
    file_id = id
    return file_id
