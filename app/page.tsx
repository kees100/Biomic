'use client' 
import React, { useState } from 'react';
import axios from 'axios';

interface BlastpResult {
  accession_number: string;
  blast_results: {
    BlastOutput2: {
      report: {
        results: {
          search: {
            hits: {
              description: {
                id: string;
                title: string;
                accession: string;
                taxid: number;
              }[];
            }[];
          };
        };
      };
    }[];
  };
}

export default function BlastpSearchPage() {
  const [accessionNumber, setAccessionNumber] = useState('');
  const [blastpResult, setBlastpResult] = useState<BlastpResult | null>(null);
  const [error, setError] = useState('');

  const handleSearch = async () => {
    try {
      const response = await axios.get<BlastpResult>(`/api/blastp/${accessionNumber}`);
      setBlastpResult(response.data);
      setError('');
    } catch (error) {
      setError('Error fetching blastp results');
      console.error(error);
    }
  };

  return (
    <div>
      <h1>Blastp Search</h1>
      <input
        type="text"
        value={accessionNumber}
        onChange={(e) => setAccessionNumber(e.target.value)}
        placeholder="Enter PDB Accession Number"
      />
      <button onClick={handleSearch}>Search</button>
      {error && <p>{error}</p>}
      {blastpResult && (
        <div>
          <h2>Result</h2>
          <table style={{ borderCollapse: 'collapse', width: '100%' }}>
            <thead>
              <tr style={{ backgroundColor: '#f2f2f2' }}>
                <th style={{ border: '1px solid #dddddd', padding: '8px', textAlign: 'left' }}>LOCUS</th>
                <th style={{ border: '1px solid #dddddd', padding: '8px', textAlign: 'left' }}>DEFINITION</th>
                <th style={{ border: '1px solid #dddddd', padding: '8px', textAlign: 'left' }}>ACCESSION</th>
                <th style={{ border: '1px solid #dddddd', padding: '8px', textAlign: 'left' }}>DBSOURCE</th>
              </tr>
            </thead>
            <tbody>
              {blastpResult.blast_results.BlastOutput2[0].report.results.search.hits.map((hit, index) => (
                <tr key={index} style={{ backgroundColor: index % 2 === 0 ? '#ffffff' : '#dddddd' }}>
                  <td style={{ border: '1px solid #dddddd', padding: '8px', textAlign: 'left' }}>{hit.description[0].id}</td>
                  <td style={{ border: '1px solid #dddddd', padding: '8px', textAlign: 'left' }}>{hit.description[0].title}</td>
                  <td style={{ border: '1px solid #dddddd', padding: '8px', textAlign: 'left' }}>{hit.description[0].accession}</td>
                  <td style={{ border: '1px solid #dddddd', padding: '8px', textAlign: 'left' }}>{hit.description[0].taxid}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
    </div>
  );
}
 