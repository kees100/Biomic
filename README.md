## Develop

Primer has de crear l'entorn de treball:

```sh
./init.sh
source ~/.bashrc
```
```./blast.sh

```

Si cal, hauràs de donar permisos d'execució:
box@bio-w4:~/m14-w4$ 
```
chmod +x init.sh 
chmod +x blast.sh 
```

## Si no s'instal·la bé l'npm, cal instal·lar-lo manualment

```
sudo apt install npm
```


## Executa l'aplicació:
//Perquè funcioni s'han d'obrir els dos terminals 

```sh
npm run next-dev
npm run flask-dev
```

# No he pogut automatitzar la actualització de la base de dades. Així que ho he fet manual

Primer he entrar al docker: 
docker run --rm -it -v $PWD/blast/blastdb:/blast/blastdb -v $PWD/blast/blastdb_custom:/blast/blastdb_custom -v $PWD/blast/fasta:/blast/fasta -v $PWD/blast/queries:/blast/queries -v $PWD/blast/results:/blast/results ncbi/blast /bin/bash

Després he entrat al directori

cd /blastdb

Finalment he fer la actualització amb la següent comanda: 

update_blastdb.pl -source gcp pdbaa


# Pots provar que funciona a la terminal per exemple

"http GET http://localhost:5000/api/blastp/NP_115954"

# També pots fer una cerca des del client

"http://localhost:3000/"

Per construir una imatge

```sh
./build.sh
```



